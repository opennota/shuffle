// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// +build !amd64

package shuffle

// Uint32x16 shuffles bytes in a slice of uint32s in blocks of 16.
func Uint32x16(p []uint32) {
	uint32x16(p)
}

// Uint32x32 shuffles bytes in a slice of uint32s in blocks of 32.
func Uint32x32(p []uint32) {
	uint32x32(p)
}

// UnshuffleUint32x16 unshuffles bytes in a slice of uint32s previously shuffled by Uint32x16.
func UnshuffleUint32x16(p []uint32) {
	unshuffleUint32x16(p)
}

// UnshuffleUint32x32 unshuffles bytes in a slice of uint32s previously shuffled by Uint32x32.
func UnshuffleUint32x32(p []uint32) {
	unshuffleUint32x32(p)
}
