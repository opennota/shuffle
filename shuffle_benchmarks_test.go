// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package shuffle

import (
	"runtime"
	"testing"
)

var buf = make([]uint32, 65536)

func BenchmarkUint32x16Generic(b *testing.B) {
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		uint32x16(buf)
	}
}

func BenchmarkShuffleUint32x32Generic(b *testing.B) {
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		uint32x32(buf)
	}
}

func BenchmarkUint32x16SSE2(b *testing.B) {
	if runtime.GOARCH != "amd64" {
		b.SkipNow()
	}
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		Uint32x16(buf)
	}
}

func BenchmarkUint32x16AVX2(b *testing.B) {
	if !avx2 {
		b.SkipNow()
	}
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		Uint32x32(buf)
	}
}

func BenchmarkUnshuffleUint32x16Generic(b *testing.B) {
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		unshuffleUint32x16(buf)
	}
}

func BenchmarkUnshuffleUint32x32Generic(b *testing.B) {
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		unshuffleUint32x32(buf)
	}
}

func BenchmarkUnshuffleUint32x16SSE2(b *testing.B) {
	if runtime.GOARCH != "amd64" {
		b.SkipNow()
	}
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		UnshuffleUint32x16(buf)
	}
}

func BenchmarkUnshuffleUint32x16AVX2(b *testing.B) {
	if !avx2 {
		b.SkipNow()
	}
	b.SetBytes(int64(len(buf)))
	for i := 0; i < b.N; i++ {
		UnshuffleUint32x32(buf)
	}
}
