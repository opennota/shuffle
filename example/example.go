package main

import (
	"fmt"
	"log"
	"math/rand"
	"reflect"
	"sort"
	"time"
	"unsafe"

	"github.com/golang/snappy"

	"gitlab.com/opennota/shuffle"
)

func measure(a []uint32) (float64, time.Duration, time.Duration) {
	hdr := reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(&a[0])),
		Len:  len(a) * 4,
		Cap:  len(a) * 4,
	}
	src := *(*[]byte)(unsafe.Pointer(&hdr))
	dst := make([]byte, snappy.MaxEncodedLen(len(src)))
	decodeDst := make([]byte, len(src))

	start := time.Now()
	encoded := snappy.Encode(dst, src)
	encodingTime := time.Since(start)

	start = time.Now()
	_, err := snappy.Decode(decodeDst, encoded)
	if err != nil {
		log.Fatal(err)
	}
	decodingTime := time.Since(start)

	return 100 * float64(len(encoded)) / float64(len(src)), encodingTime, decodingTime
}

type T []uint32

func (t T) Len() int           { return len(t) }
func (t T) Less(i, j int) bool { return t[i] < t[j] }
func (t T) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }

func main() {
	rand.Seed(time.Now().Unix())

	fmt.Println("random uint32s:")
	for _, n := range []int{10000, 100000, 1000000, 10000000} {
		fmt.Printf("%10d  -", n)
		var a []uint32
		for i := 0; i < n; i++ {
			a = append(a, uint32(rand.Int31()))
		}
		pct, et, dt := measure(a)
		fmt.Printf("  ratio: %.2f%%", pct)
		fmt.Printf("  enc: %v", et)
		fmt.Printf("  dec: %v", dt)
		fmt.Println()
	}
	fmt.Println()

	fmt.Println("random uint32s, sorted:")
	for _, n := range []int{10000, 100000, 1000000, 10000000} {
		fmt.Printf("%10d  -", n)
		var a []uint32
		for i := 0; i < n; i++ {
			a = append(a, uint32(rand.Int31()))
		}
		start := time.Now()
		sort.Sort(T(a))
		sortTime := time.Since(start)
		pct, et, dt := measure(a)
		fmt.Printf("  ratio: %.2f%%", pct)
		fmt.Printf("  enc: %v", et)
		fmt.Printf("  dec: %v", dt)
		fmt.Printf("  sort: %v", sortTime)
		fmt.Println()
	}
	fmt.Println()

	fmt.Println("random uint32s, differences:")
	for _, n := range []int{10000, 100000, 1000000, 10000000} {
		fmt.Printf("%10d  -", n)
		var a []uint32
		for i := 0; i < n; i++ {
			a = append(a, uint32(rand.Int31()))
		}
		start := time.Now()
		sort.Sort(T(a))
		for i := n - 1; i > 0; i-- {
			a[i] -= a[i-1]
		}
		sortTime := time.Since(start)
		pct, et, dt := measure(a)
		start = time.Now()
		for i := 1; i < n; i++ {
			a[i] += a[i-1]
		}
		undiffTime := time.Since(start)
		fmt.Printf("  ratio: %.2f%%", pct)
		fmt.Printf("  enc: %v", et)
		fmt.Printf("  dec: %v", dt)
		fmt.Printf("  sort/diff: %v", sortTime)
		fmt.Printf("  undiff: %v", undiffTime)
		fmt.Println()
	}
	fmt.Println()

	fmt.Println("random uint32s, sorted and shuffled:")
	for _, n := range []int{10000, 100000, 1000000, 10000000} {
		fmt.Printf("%10d  -", n)
		var a []uint32
		for i := 0; i < n; i++ {
			a = append(a, uint32(rand.Int31()))
		}
		start := time.Now()
		sort.Sort(T(a))
		shuffle.Uint32x32(a)
		sortTime := time.Since(start)
		pct, et, dt := measure(a)
		start = time.Now()
		shuffle.UnshuffleUint32x32(a)
		unshuffleTime := time.Since(start)
		fmt.Printf("  ratio: %.2f%%", pct)
		fmt.Printf("  enc: %v", et)
		fmt.Printf("  dec: %v", dt)
		fmt.Printf("  sort/shuffle: %v", sortTime)
		fmt.Printf("  unshuffle: %v", unshuffleTime)
		fmt.Println()
	}
	fmt.Println()

	fmt.Println("random uint32s, differences shuffled:")
	for _, n := range []int{10000, 100000, 1000000, 10000000} {
		fmt.Printf("%10d  -", n)
		var a []uint32
		for i := 0; i < n; i++ {
			a = append(a, uint32(rand.Int31()))
		}
		start := time.Now()
		sort.Sort(T(a))
		for i := n - 1; i > 0; i-- {
			a[i] -= a[i-1]
		}
		shuffle.Uint32x32(a)
		sortTime := time.Since(start)
		pct, et, dt := measure(a)
		start = time.Now()
		shuffle.UnshuffleUint32x32(a)
		for i := 1; i < n; i++ {
			a[i] += a[i-1]
		}
		unshuffleTime := time.Since(start)
		fmt.Printf("  ratio: %.2f%%", pct)
		fmt.Printf("  enc: %v", et)
		fmt.Printf("  dec: %v", dt)
		fmt.Printf("  sort/diff/shuffle: %v", sortTime)
		fmt.Printf("  unshuffle/undiff: %v", unshuffleTime)
		fmt.Println()
	}
}
