// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// func haveAVX2() bool
TEXT ·haveAVX2(SB),0,$0-8
        MOVQ $7, AX
        XORQ CX, CX
        CPUID
        SHRQ $5, BX
        XORQ AX, AX
        ANDQ $1, BX
        JZ _no_avx2
        MOVQ $1, AX
_no_avx2:
        MOVB AX, ret+0(FP)
        RET

// func shuf4x32bAVX2(p unsafe.Pointer)
TEXT ·shuf4x32bAVX2(SB),0,$0-8
        MOVQ p+0(FP), SI
        MOVQ SI, DI
        MOVQ $32, CX
        XORQ AX, AX

        // vmovdqu 0x2(%rip),%ymm7
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x3d; BYTE $0x02; BYTE $0x00; BYTE $0x00; BYTE $0x00

        // jmp short _l1
        BYTE $0xeb; BYTE $0x20

        BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x4; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x1; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x5; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x2; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x6; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x3; BYTE $0x0; BYTE $0x0; BYTE $0x0; BYTE $0x7; BYTE $0x0; BYTE $0x0; BYTE $0x0

_l1:
        // vmovdqu (%rsi),%ymm3
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x1e
        SUBQ $0xffffffffffffff80, SI
        // vmovdqu -0x60(%rsi),%ymm6
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x76; BYTE $0xa0
        // vpshufd $0xd8,%ymm3,%ymm1
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xcb; BYTE $0xd8
        // vpshufd $0x8d,%ymm3,%ymm3
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xdb; BYTE $0x8d
        // vpunpcklbw %ymm3,%ymm1,%ymm0
        BYTE $0xc5; BYTE $0xf5; BYTE $0x60; BYTE $0xc3
        // vpshufd $0xd8,%ymm6,%ymm1
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xce; BYTE $0xd8
        // vpshufd $0x8d,%ymm6,%ymm6
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xf6; BYTE $0x8d
        // vpshufd $0x4e,%ymm0,%ymm2
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xd0; BYTE $0x4e
        // vpunpcklwd %ymm2,%ymm0,%ymm3
        BYTE $0xc5; BYTE $0xfd; BYTE $0x61; BYTE $0xda
        // vpunpcklbw %ymm6,%ymm1,%ymm0
        BYTE $0xc5; BYTE $0xf5; BYTE $0x60; BYTE $0xc6
        // vmovdqu -0x40(%rsi),%ymm1
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x4e; BYTE $0xc0
        // vpshufd $0x4e,%ymm0,%ymm2
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xd0; BYTE $0x4e
        // vpunpcklwd %ymm2,%ymm0,%ymm6
        BYTE $0xc5; BYTE $0xfd; BYTE $0x61; BYTE $0xf2
        // vpshufd $0xd8,%ymm1,%ymm2
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xd1; BYTE $0xd8
        // vpshufd $0x8d,%ymm1,%ymm1
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xc9; BYTE $0x8d
        // vpunpcklbw %ymm1,%ymm2,%ymm1
        BYTE $0xc5; BYTE $0xed; BYTE $0x60; BYTE $0xc9
        // vpunpckhdq %ymm6,%ymm3,%ymm2
        BYTE $0xc5; BYTE $0xe5; BYTE $0x6a; BYTE $0xd6
        // vpshufd $0x4e,%ymm1,%ymm0
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xc1; BYTE $0x4e
        // vpunpcklwd %ymm0,%ymm1,%ymm1
        BYTE $0xc5; BYTE $0xf5; BYTE $0x61; BYTE $0xc8
        // vmovdqu -0x20(%rsi),%ymm0
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x46; BYTE $0xe0
        // vpshufd $0xd8,%ymm0,%ymm5
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xe8; BYTE $0xd8
        // vpshufd $0x8d,%ymm0,%ymm0
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xc0; BYTE $0x8d
        // vpunpcklbw %ymm0,%ymm5,%ymm4
        BYTE $0xc5; BYTE $0xd5; BYTE $0x60; BYTE $0xe0
        // vpshufd $0x4e,%ymm4,%ymm0
        BYTE $0xc5; BYTE $0xfd; BYTE $0x70; BYTE $0xc4; BYTE $0x4e
        // vpunpcklwd %ymm0,%ymm4,%ymm0
        BYTE $0xc5; BYTE $0xdd; BYTE $0x61; BYTE $0xc0
        // vpunpckldq %ymm6,%ymm3,%ymm4
        BYTE $0xc5; BYTE $0xe5; BYTE $0x62; BYTE $0xe6
        // vpunpckldq %ymm0,%ymm1,%ymm5
        BYTE $0xc5; BYTE $0xf5; BYTE $0x62; BYTE $0xe8
        // vpunpckhdq %ymm0,%ymm1,%ymm0
        BYTE $0xc5; BYTE $0xf5; BYTE $0x6a; BYTE $0xc0
        // vpunpcklqdq %ymm5,%ymm4,%ymm3
        BYTE $0xc5; BYTE $0xdd; BYTE $0x6c; BYTE $0xdd
        // vpunpckhqdq %ymm5,%ymm4,%ymm4
        BYTE $0xc5; BYTE $0xdd; BYTE $0x6d; BYTE $0xe5
        // vpunpcklqdq %ymm0,%ymm2,%ymm1
        BYTE $0xc5; BYTE $0xed; BYTE $0x6c; BYTE $0xc8
        // vpunpckhqdq %ymm0,%ymm2,%ymm0
        BYTE $0xc5; BYTE $0xed; BYTE $0x6d; BYTE $0xc0
        // vpermd %ymm4,%ymm7,%ymm4
        BYTE $0xc4; BYTE $0xe2; BYTE $0x45; BYTE $0x36; BYTE $0xe4
        // vpermd %ymm3,%ymm7,%ymm2
        BYTE $0xc4; BYTE $0xe2; BYTE $0x45; BYTE $0x36; BYTE $0xd3
        // vpermd %ymm1,%ymm7,%ymm1
        BYTE $0xc4; BYTE $0xe2; BYTE $0x45; BYTE $0x36; BYTE $0xc9
        // vpermd %ymm0,%ymm7,%ymm0
        BYTE $0xc4; BYTE $0xe2; BYTE $0x45; BYTE $0x36; BYTE $0xc0
        // vmovdqu %ymm2,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x14; BYTE $0x07
        ADDQ CX, AX
        // vmovdqu %ymm4,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x24; BYTE $0x07
        ADDQ CX, AX
        // vmovdqu %ymm1,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x0c; BYTE $0x07
        ADDQ CX, AX
        // vmovdqu %ymm0,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x04; BYTE $0x07
        RET

// func unshuf4x32bAVX2(p unsafe.Pointer)
TEXT ·unshuf4x32bAVX2(SB),0,$0-8
        MOVQ p+0(FP), SI
        MOVQ SI, DI
        MOVQ $32, CX
        XORQ AX, AX
        // vmovdqu (%rsi,%rax,1),%ymm2
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x14; BYTE $0x06
        ADDQ CX, AX
        SUBQ $0xffffffffffffff80, DI
        // vmovdqu (%rsi,%rax,1),%ymm1
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x0c; BYTE $0x06
        ADDQ CX, AX
        // vmovdqu (%rsi,%rax,1),%ymm4
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x24; BYTE $0x06
        ADDQ CX, AX
        // vmovdqu (%rsi,%rax,1),%ymm0
        BYTE $0xc5; BYTE $0xfe; BYTE $0x6f; BYTE $0x04; BYTE $0x06
        // vpunpcklbw %ymm1,%ymm2,%ymm3
        BYTE $0xc5; BYTE $0xed; BYTE $0x60; BYTE $0xd9
        // vpunpckhbw %ymm1,%ymm2,%ymm1
        BYTE $0xc5; BYTE $0xed; BYTE $0x68; BYTE $0xc9
        // vpunpcklbw %ymm0,%ymm4,%ymm2
        BYTE $0xc5; BYTE $0xdd; BYTE $0x60; BYTE $0xd0
        // vpunpckhbw %ymm0,%ymm4,%ymm0
        BYTE $0xc5; BYTE $0xdd; BYTE $0x68; BYTE $0xc0
        // vpunpcklwd %ymm2,%ymm3,%ymm4
        BYTE $0xc5; BYTE $0xe5; BYTE $0x61; BYTE $0xe2
        // vpunpckhwd %ymm2,%ymm3,%ymm2
        BYTE $0xc5; BYTE $0xe5; BYTE $0x69; BYTE $0xd2
        // vpunpcklwd %ymm0,%ymm1,%ymm3
        BYTE $0xc5; BYTE $0xf5; BYTE $0x61; BYTE $0xd8
        // vpunpckhwd %ymm0,%ymm1,%ymm0
        BYTE $0xc5; BYTE $0xf5; BYTE $0x69; BYTE $0xc0
        // vperm2i128 $0x20,%ymm2,%ymm4,%ymm5
        BYTE $0xc4; BYTE $0xe3; BYTE $0x5d; BYTE $0x46; BYTE $0xea; BYTE $0x20
        // vperm2i128 $0x31,%ymm2,%ymm4,%ymm2
        BYTE $0xc4; BYTE $0xe3; BYTE $0x5d; BYTE $0x46; BYTE $0xd2; BYTE $0x31
        // vperm2i128 $0x20,%ymm0,%ymm3,%ymm1
        BYTE $0xc4; BYTE $0xe3; BYTE $0x65; BYTE $0x46; BYTE $0xc8; BYTE $0x20
        // vperm2i128 $0x31,%ymm0,%ymm3,%ymm0
        BYTE $0xc4; BYTE $0xe3; BYTE $0x65; BYTE $0x46; BYTE $0xc0; BYTE $0x31
        // vmovdqu %ymm5,-0x80(%rdi)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x6f; BYTE $0x80
        // vmovdqu %ymm2,-0x40(%rdi)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x57; BYTE $0xc0
        // vmovdqu %ymm1,-0x60(%rdi)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x4f; BYTE $0xa0
        // vmovdqu %ymm0,-0x20(%rdi)
        BYTE $0xc5; BYTE $0xfe; BYTE $0x7f; BYTE $0x47; BYTE $0xe0
        RET
