// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// +build amd64

package shuffle

import "unsafe"

func haveAVX2() bool
func shuf4x32bAVX2(unsafe.Pointer)
func unshuf4x32bAVX2(unsafe.Pointer)

var avx2 = haveAVX2()

// Uint32x32 shuffles bytes in a slice of uint32s in blocks of 32.
func Uint32x32(p []uint32) {
	if avx2 {
		n := len(p) - len(p)%32
		for i := 0; i < n; i += 32 {
			shuf4x32bAVX2(unsafe.Pointer(&p[i]))
		}
		uint32x32rest(p[n:])
	} else {
		uint32x32(p)
	}

}

// UnshuffleUint32x32 unshuffles bytes in a slice of uint32s previously shuffled by Uint32x32.
func UnshuffleUint32x32(p []uint32) {
	if avx2 {
		n := len(p) - len(p)%32
		for i := 0; i < n; i += 32 {
			unshuf4x32bAVX2(unsafe.Pointer(&p[i]))
		}
		unshuffleUint32x32rest(p[n:])
	} else {
		unshuffleUint32x32(p)
	}

}
