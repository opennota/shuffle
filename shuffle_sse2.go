// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// +build amd64

package shuffle

import "unsafe"

func shuf4x16bSSE2(unsafe.Pointer)
func unshuf4x16bSSE2(unsafe.Pointer)

// Uint32x16 shuffles bytes in a slice of uint32s in blocks of 16.
func Uint32x16(p []uint32) {
	n := len(p) - len(p)%16
	for i := 0; i < n; i += 16 {
		shuf4x16bSSE2(unsafe.Pointer(&p[i]))
	}
	uint32x16rest(p[n:])
}

// UnshuffleUint32x16 unshuffles bytes in a slice of uint32s previously shuffled by Uint32x16.
func UnshuffleUint32x16(p []uint32) {
	n := len(p) - len(p)%16
	for i := 0; i < n; i += 16 {
		unshuf4x16bSSE2(unsafe.Pointer(&p[i]))
	}
	unshuffleUint32x16rest(p[n:])
}
