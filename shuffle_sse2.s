// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// func shuf4x16bSSE2(p unsafe.Pointer)
TEXT ·shuf4x16bSSE2(SB),0,$0-8
        MOVQ p+0(FP), SI
        MOVQ SI, DI
        MOVQ $16, CX
        XORQ AX, AX
        // vmovdqu (%rsi),%xmm3
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x1e
        ADDQ $0x40, SI
        // vpshufd $0xd8,%xmm3,%xmm1
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xcb; BYTE $0xd8
        // vpshufd $0x8d,%xmm3,%xmm3
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xdb; BYTE $0x8d
        // vmovdqu -0x30(%rsi),%xmm6
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x76; BYTE $0xd0
        // vpunpcklbw %xmm3,%xmm1,%xmm0
        BYTE $0xc5; BYTE $0xf1; BYTE $0x60; BYTE $0xc3
        // vpshufd $0xd8,%xmm6,%xmm1
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xce; BYTE $0xd8
        // vpshufd $0x8d,%xmm6,%xmm6
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xf6; BYTE $0x8d
        // vpshufd $0x4e,%xmm0,%xmm2
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xd0; BYTE $0x4e
        // vpunpcklwd %xmm2,%xmm0,%xmm3
        BYTE $0xc5; BYTE $0xf9; BYTE $0x61; BYTE $0xda
        // vpunpcklbw %xmm6,%xmm1,%xmm0
        BYTE $0xc5; BYTE $0xf1; BYTE $0x60; BYTE $0xc6
        // vmovdqu -0x20(%rsi),%xmm1
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x4e; BYTE $0xe0
        // vpshufd $0x4e,%xmm0,%xmm2
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xd0; BYTE $0x4e
        // vpunpcklwd %xmm2,%xmm0,%xmm6
        BYTE $0xc5; BYTE $0xf9; BYTE $0x61; BYTE $0xf2
        // vpshufd $0xd8,%xmm1,%xmm2
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xd1; BYTE $0xd8
        // vpshufd $0x8d,%xmm1,%xmm1
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xc9; BYTE $0x8d
        // vpunpcklbw %xmm1,%xmm2,%xmm1
        BYTE $0xc5; BYTE $0xe9; BYTE $0x60; BYTE $0xc9
        // vpunpckhdq %xmm6,%xmm3,%xmm2
        BYTE $0xc5; BYTE $0xe1; BYTE $0x6a; BYTE $0xd6
        // vpshufd $0x4e,%xmm1,%xmm0
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xc1; BYTE $0x4e
        // vpunpcklwd %xmm0,%xmm1,%xmm1
        BYTE $0xc5; BYTE $0xf1; BYTE $0x61; BYTE $0xc8
        // vmovdqu -0x10(%rsi),%xmm0
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x46; BYTE $0xf0
        // vpshufd $0xd8,%xmm0,%xmm5
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xe8; BYTE $0xd8
        // vpshufd $0x8d,%xmm0,%xmm0
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xc0; BYTE $0x8d
        // vpunpcklbw %xmm0,%xmm5,%xmm4
        BYTE $0xc5; BYTE $0xd1; BYTE $0x60; BYTE $0xe0
        // vpshufd $0x4e,%xmm4,%xmm0
        BYTE $0xc5; BYTE $0xf9; BYTE $0x70; BYTE $0xc4; BYTE $0x4e
        // vpunpcklwd %xmm0,%xmm4,%xmm0
        BYTE $0xc5; BYTE $0xd9; BYTE $0x61; BYTE $0xc0
        // vpunpckldq %xmm6,%xmm3,%xmm4
        BYTE $0xc5; BYTE $0xe1; BYTE $0x62; BYTE $0xe6
        // vpunpckldq %xmm0,%xmm1,%xmm3
        BYTE $0xc5; BYTE $0xf1; BYTE $0x62; BYTE $0xd8
        // vpunpckhdq %xmm0,%xmm1,%xmm0
        BYTE $0xc5; BYTE $0xf1; BYTE $0x6a; BYTE $0xc0
        // vpunpcklqdq %xmm3,%xmm4,%xmm5
        BYTE $0xc5; BYTE $0xd9; BYTE $0x6c; BYTE $0xeb
        // vpunpckhqdq %xmm3,%xmm4,%xmm4
        BYTE $0xc5; BYTE $0xd9; BYTE $0x6d; BYTE $0xe3
        // vpunpcklqdq %xmm0,%xmm2,%xmm1
        BYTE $0xc5; BYTE $0xe9; BYTE $0x6c; BYTE $0xc8
        // vpunpckhqdq %xmm0,%xmm2,%xmm0
        BYTE $0xc5; BYTE $0xe9; BYTE $0x6d; BYTE $0xc0
        // vmovups %xmm5,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x2c; BYTE $0x07
        ADDQ CX, AX
        // vmovups %xmm4,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x24; BYTE $0x07
        ADDQ CX, AX
        // vmovups %xmm1,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x0c; BYTE $0x07
        ADDQ CX, AX
        // vmovups %xmm0,(%rdi,%rax,1)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x04; BYTE $0x07
        RET

// func unshuf4x16bSSE2(p unsafe.Pointer)
TEXT ·unshuf4x16bSSE2(SB),0,$0-8
        MOVQ p+0(FP), SI
        MOVQ SI, DI
        MOVQ $16, CX
        XORQ AX, AX
        // vmovdqu (%rsi,%rax,1),%xmm1
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x0c; BYTE $0x06
        ADDQ CX, AX
        ADDQ $0x40, DI
        // vmovdqu (%rsi,%rax,1),%xmm0
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x04; BYTE $0x06
        ADDQ CX, AX
        // vmovdqu (%rsi,%rax,1),%xmm2
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x14; BYTE $0x06
        ADDQ CX, AX
        // vpunpcklbw %xmm0,%xmm1,%xmm3
        BYTE $0xc5; BYTE $0xf1; BYTE $0x60; BYTE $0xd8
        // vmovdqu (%rsi,%rax,1),%xmm5
        BYTE $0xc5; BYTE $0xfa; BYTE $0x6f; BYTE $0x2c; BYTE $0x06
        // vpunpckhbw %xmm0,%xmm1,%xmm0
        BYTE $0xc5; BYTE $0xf1; BYTE $0x68; BYTE $0xc0
        // vpunpcklbw %xmm5,%xmm2,%xmm4
        BYTE $0xc5; BYTE $0xe9; BYTE $0x60; BYTE $0xe5
        // vpunpckhbw %xmm5,%xmm2,%xmm1
        BYTE $0xc5; BYTE $0xe9; BYTE $0x68; BYTE $0xcd
        // vpunpcklwd %xmm4,%xmm3,%xmm5
        BYTE $0xc5; BYTE $0xe1; BYTE $0x61; BYTE $0xec
        // vpunpcklwd %xmm1,%xmm0,%xmm2
        BYTE $0xc5; BYTE $0xf9; BYTE $0x61; BYTE $0xd1
        // vpunpckhwd %xmm4,%xmm3,%xmm3
        BYTE $0xc5; BYTE $0xe1; BYTE $0x69; BYTE $0xdc
        // vpunpckhwd %xmm1,%xmm0,%xmm0
        BYTE $0xc5; BYTE $0xf9; BYTE $0x69; BYTE $0xc1
        // vmovups %xmm5,-0x40(%rdi)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x6f; BYTE $0xc0
        // vmovups %xmm3,-0x30(%rdi)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x5f; BYTE $0xd0
        // vmovups %xmm2,-0x20(%rdi)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x57; BYTE $0xe0
        // vmovups %xmm0,-0x10(%rdi)
        BYTE $0xc5; BYTE $0xf8; BYTE $0x11; BYTE $0x47; BYTE $0xf0
        RET
