shuffle [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/shuffle?status.svg)](http://godoc.org/gitlab.com/opennota/shuffle) [![Pipeline status](https://gitlab.com/opennota/shuffle/badges/master/pipeline.svg)](https://gitlab.com/opennota/shuffle/commits/master)
=======

Package shuffle provides functions for shuffling bytes in blocks of uint32s for better compression.  It is one of the techniques used by [blosc](http://www.blosc.org/).


## Install

    go get -u gitlab.com/opennota/shuffle

## Use

See an [example](https://gitlab.com/opennota/shuffle/blob/master/example/example.go).
